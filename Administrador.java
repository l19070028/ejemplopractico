/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.banco;

/**
 *
 * @author Israel
 */

public class Administrador extends Funcionario implements FuncionarioAutenticable{
private AutenticacionUtil util;
public Administrador(){
    this.util = new AutenticacionUtil();
}

    @Override
    public double getBonificacion() { 
        return this.getSalario();
    }

    @Override
    public void setClave(String clave) {
       this.util.setClave(clave);
    }
//
    @Override
    public boolean iniciarSesion(String clave) {
        return this.util.iniciarSesio(clave);
    }
    
}
